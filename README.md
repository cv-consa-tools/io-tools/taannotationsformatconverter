# TAAnnotationsFormatConverter
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-orange.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![C++](https://img.shields.io/badge/C++-Solutions-blue.svg?style=flat&logo=c%2B%2B)](https://isocpp.org/)
[![pipeline status](https://gitlab.com/cv-consa-tools/io-tools/taannotationsformatconverter/badges/master/pipeline.svg)](https://gitlab.com/cv-consa-tools/io-tools/taannotationsformatconverter/commits/master)
[![](https://tokei.rs/b1/gitlab/cv-consa-tools%2Fio-tools/taannotationsformatconverter?category=lines)](https://gitlab.com/cv-consa-tools/io-tools/taannotationsformatconverter/-/edit/master/README.md).

Project for converting face aligment labels and metadata from TXT files to XML files. 


### Annotations

The project is thought for converting face rectangles and facial landmarks annotations from TXT files to XML files, for example, used in the DLIB library to train its Face Landmark Detector ( http://blog.dlib.net/2018/01/correctly-mirroring-datasets.html ).

The protocol followed in the landmarks is the ibug protocol, with 68 landmark points.
(https://ibug.doc.ic.ac.uk/resources/facial-point-annotations/)

